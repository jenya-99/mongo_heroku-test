// Получение всех товаров:
function GetProducts() {
  $.ajax({
    url: "/products",
    type: "GET",
    contentType: "application/json",
    success: function(products) {
      let rows = "";
      $.each(products, function (index, product) {
        // добавляем полученные элементы в таблицу
        rows += row(product);
      });
      $("table tbody").append(rows);
      }
  });
}
// создание строки для таблицы
function row(product) {
  return "<tr data-rowid='" + product._id + "'>" +
          "<td>" + product._id + "</td>" + 
          "<td>" + product.code + "</td>" +
          "<td>" + product.name + "</td>" +
          "<td>" + product.qnt + "</td>" +
          "<td><a class='editLink' data-id='" + product._id + "'>Изменить</a> | " +
          "<a class='removeLink' data-id='" + product._id + "'>Удалить</a></td></tr>";
}

// Получение одного товара:
function GetProduct(id) {
  $.ajax({
    url: "/products/" + id,
    type: "GET",
    contentType: "application/json",
    success: function(product) {
      let form = document.forms["form"];
      form.elements["id"].value = product._id;
      form.elements["code"].value = product.code;
      form.elements["name"].value = product.name;
      form.elements["qnt"].value = product.qnt;
    }
  });
}
// Добавление товара:
function CreateProduct(productCode, productName, productQnt) {
  $.ajax({
    url: "/products",
    contentType: "application/json",
    method: "POST",
    data: JSON.stringify({
      code: productCode,
      name: productName,
      qnt: productQnt
    }),
    success: function (product) {
      reset();
      $("table tbody").append(row(product));
    }
  });
}
// Изменение пользователя:
function EditProduct(productId, productCode, productName, productQnt) {
  $.ajax({
    url: "/products",
    contentType: "application/json",
    method: "PUT",
    data: JSON.stringify({
      id: productId,
      code: productCode,
      name: productName,
      qnt: productQnt
    }),
    success: function(product) {
      reset();
      console.log(product);
      $("tr[data-rowid='" + product._id + "']").replaceWith(row(product));
    }
  })
}
// Удаление пользователя:
function DeleteProduct(id) {
  $.ajax({
    url: "/products/" + id,
    contentType: "application/json",
    method: "DELETE",
    success: function(product) {
      console.log(product);
      $("tr[data-rowid='" + product._id + "']").remove();
    }
  });
}
// сброс формы:
function reset() {
  let form = document.forms["form"];
  form.reset();
  form.elements["id"].value = 0;
}